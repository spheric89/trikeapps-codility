class UsefulRules
  attr_reader :transformed_string

  RULES = {ab_to_ac: {from: "AB", to: "AC"},
           ba_to_aa: {from: "BA", to: "AA"},
           cb_to_cc: {from: "CB", to: "CC"},
           bc_to_cc: {from: "BC", to: "CC"},
           aa_to_a: {from: "AA", to: "A"},
           cc_to_c: {from: "CC", to: "C"}}

  def initialize(s)
    @transformed_string = s
  end

  def apply_transform
    loop do
      @useful_rules = RULES.dup
      rule = find_rand_rule
      rule ? apply_rule(rule) : return
    end
  end

private

  def find_rand_rule
    loop do
      @useful_rules.empty? ? return : rule_name = @useful_rules.keys.sample
      if @transformed_string =~ /#{@useful_rules[rule_name][:from]}/
        return @useful_rules[rule_name]
      end
      @useful_rules.delete(rule_name)
    end
  end

  def apply_rule(rule)
    @transformed_string[rule.fetch(:from)] = rule.fetch(:to)
  end
end

def solution(s)
  raise ArgumentError unless s.is_a? String
  rules = UsefulRules.new(s)
  rules.apply_transform
  return rules.transformed_string
end
