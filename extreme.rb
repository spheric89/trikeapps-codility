class ExtremeArray

  def initialize(a)
    @array = a
    @max_deviation = {devi: 0, index: 0}
  end

  def find_extreme_deviation
    raise ArgumentError unless @array.is_a? Array
    @array.empty? ? -1 : calculate
  end

private

  def calculate
    @array.each_with_index do |ele, index|
      devi = deviation(ele)
      if devi >= @max_deviation.fetch(:devi)
        @max_deviation.merge!(devi: devi, index: index)
      end
    end
    @max_deviation.fetch(:index)
  end

  def average
    @average ||= @array.reduce(&:+) / @array.size
  end

  def deviation(ele)
    average > ele ? average - ele : ele - average
  end

end

def solution(a)
  extreme_array = ExtremeArray.new(a)
  extreme_array.find_extreme_deviation
end
